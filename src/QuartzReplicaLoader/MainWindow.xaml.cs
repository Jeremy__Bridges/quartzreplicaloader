﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.IO.Compression;
using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.Tasks.Offline;
using Esri.ArcGISRuntime.UI;
using Microsoft.Win32;
using Geometry = Esri.ArcGISRuntime.Geometry.Geometry;

namespace QuartzReplicaLoader
{
    public partial class MainWindow
    {
        private Geodatabase _geodatabase;
        private GraphicsOverlay _geometryShowingLayer;
        private string _lastTableLoaded;

        public MainWindow()
        {
            InitializeComponent();
            MapView.Map = new Map(Basemap.CreateStreetsVector());
            _geometryShowingLayer = new GraphicsOverlay {Opacity = 0.4};
            MapView.GraphicsOverlays.Add(_geometryShowingLayer);
            MapView.LayerViewStateChanged += WhenLayerViewStateChanged;
        }

        private async void WhenLayerViewStateChanged(object sender, LayerViewStateChangedEventArgs e)
        {
            if (e.LayerViewState.Error != null)
            {
                await TellUser($"Layer '{e.Layer.Name}' couldn't load: {e.LayerViewState.Error.Message}");
            }
        }

        private async Task AddFeatureLayer()
        {
            var layer = new FeatureLayer(new Uri(
                "https://orbittest.miner.com:6443/arcgis/rest/services/GRU_ElectricDistribution_SubtypeLabels/FeatureServer/1"));
            await layer.LoadAsync();
            MapView.Map.OperationalLayers.Add(layer);
            await MapView.SetViewpointGeometryAsync(layer.FullExtent);
        }

        private void AddReplica(object sender, RoutedEventArgs e)
        {
            AddReplica();
        }

        private async Task AddReplica()
        {
            var dialog = new OpenFileDialog {Multiselect = true, Filter = "Geodatabases|*.geodatabase"};
            if ((bool) dialog.ShowDialog(this))
            {
                foreach (var fileName in dialog.FileNames)
                {
                    try
                    {
                        if (_geodatabase != null)
                        {
                            MapView.Map.OperationalLayers.Clear();
                            _geodatabase.Close();
                        }

                        Envelope totalExtent = null;
                        _geodatabase = await Geodatabase.OpenAsync(fileName);
                        foreach (var table in _geodatabase.GeodatabaseFeatureTables)
                        {
                            await table.LoadAsync();

                            totalExtent = totalExtent == null
                                ? table.Extent
                                : table.Extent == null
                                    ? totalExtent
                                    : GeometryEngine.Union(table.Extent, totalExtent).Extent;

                            MapView.Map.OperationalLayers.Add(new FeatureLayer(table) { Id = table.DisplayName ?? string.Empty });
                        }
                        if (totalExtent != null &&
                            !totalExtent.IsEmpty)
                        {
                            await MapView.SetViewpointGeometryAsync(totalExtent);
                        }
                        else if (_geodatabase.GenerateGeodatabaseExtent != null &&
                                 !_geodatabase.GenerateGeodatabaseExtent.IsEmpty)
                        {
                            await MapView.SetViewpointGeometryAsync(_geodatabase.GenerateGeodatabaseExtent);
                        }
                    }
                    catch (Exception exception)
                    {
                        await TellUser($"Couldn't open or add geodatabase to map: {exception.Message}");
                    }

                    break;
                }
            }
        }


        private void ApplyDelta(object sender, RoutedEventArgs e)
        {
            ApplyDelta();
        }

        private async Task ApplyDelta()
        {
            if (_geodatabase is null) return;

            try
            {
                var dialog = new OpenFileDialog { Multiselect = true, Filter = "Zipped Deltas|*.gz" };
                if ((bool)dialog.ShowDialog(this))
                {
                    foreach (var zipFileName in dialog.FileNames.OrderBy(f => f))
                    {
                        var deltaFileName = Decompress(new FileInfo(zipFileName));
                        await GeodatabaseSyncTask.ImportGeodatabaseDeltaAsync(
                            _geodatabase.Path, deltaFileName);
                    }
                }
            }
            catch (Exception e)
            {
                TellUser($"Could not apply deltas: {e}");
            }
            
        }

        private static string Decompress(FileInfo fileToDecompress)
        {
            var currentFileName = fileToDecompress.FullName;
            var newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

            if (File.Exists(newFileName))
            {
                return newFileName;
            }

            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine("Decompressed: {0}", fileToDecompress.Name);
                    }
                }
            }

            return newFileName;
        }

        private void AddTpk(object sender, RoutedEventArgs e)
        {
            AddTpk();
        }

        private async Task AddTpk()
        {
            var dialog = new OpenFileDialog { Multiselect = true, Filter = "TPKs|*.tpk" };
            if ((bool)dialog.ShowDialog(this))
            {
                foreach (var fileName in dialog.FileNames)
                {
                    try
                    {
                        var layer = new ArcGISTiledLayer(new TileCache(fileName));
                        await layer.LoadAsync();
                        MapView.Map.Basemap.BaseLayers.Add(layer);
                        
                        if (layer.FullExtent != null)
                        {
                            await MapView.SetViewpointGeometryAsync(layer.FullExtent);
                        }
                    }
                    catch (Exception exception)
                    {
                        await TellUser($"Couldn't load or add TPK to map: {exception.Message}");
                    }

                    break;
                }
            }
        }

        private void QueryReplica(object sender, RoutedEventArgs e)
        {
            QueryReplica();
        }

        public async Task QueryReplica()
        { 
            var geometries = new Dictionary<string, Geometry>();
            var dialog = new OpenFileDialog { Multiselect = true, Filter = "Geometries|*.geometry"};
            if ((bool) dialog.ShowDialog(this))
            {
                foreach (var filePath in dialog.FileNames)
                {
                    try
                    {
                        var geometry = Geometry.FromJson(File.ReadAllText(filePath));
                        var key = geometry.ToJson();

                        if (!geometries.ContainsKey(key))
                        {
                            geometries.Add(key, geometry);
                        }
                    }
                    catch (Exception someError)
                    {
                        await TellUser($"Couldn't load geometry from {filePath}: {someError.Message}");
                    }
                }
            }

            if (!geometries.Any()) return;

            var geometriesQueriedSuccessfully = new List<Geometry>();
            var queryDetails = new Dictionary<string, long>();
            var queryStopwatch = new Stopwatch();
            var countStopwatch = new Stopwatch();
            var queryTime = TimeSpan.Zero;
            var countTime = TimeSpan.Zero;
            var queryCount = 0;
            var geometryNumber = 1;
            var overallStopwatch = Stopwatch.StartNew();

            foreach (var geometry in geometries.Select(p => p.Value))
            {
                var allTablesQueriedSuccessfully = true;
                foreach (var table in _geodatabase.GeodatabaseFeatureTables
                    .Where(t => t.GeometryType != GeometryType.Unknown))
                {
                    try
                    {
                        var queryParams = new QueryParameters
                        {
                            MaxFeatures = int.MaxValue,
                            Geometry = geometry,
                            SpatialRelationship = SpatialRelationship.EnvelopeIntersects,
                        };

                        queryStopwatch.Restart();
                        var resultCount1 = (await table.QueryFeaturesAsync(queryParams).ConfigureAwait(false)).ToArray().Length;
                        queryStopwatch.Stop();

                        countStopwatch.Restart();
                        var resultCount2 = await table.QueryFeatureCountAsync(queryParams).ConfigureAwait(false);
                        countStopwatch.Stop();

                        Debug.WriteLine(
                            $"Queried {table.DisplayName} with geometry {geometryNumber}, got {resultCount1} record(s)");
                        Debug.WriteLineIf(
                            resultCount1 != resultCount2,
                            $"COUNT DOESN'T MATCH FOR {table.DisplayName} -- 1: {resultCount1}, 2: {resultCount2}");

                        var tableName = table.DisplayName;
                        var tableNumber = 2;
                        while (queryDetails.ContainsKey(tableName))
                        {
                            tableName = tableName + tableNumber;
                        }

                        queryDetails.Add(tableName, resultCount1);
                        queryTime += queryStopwatch.Elapsed;
                        countTime += countStopwatch.Elapsed;
                        queryCount++;
                    }
                    catch
                    {
                        allTablesQueriedSuccessfully = false;
                    }
                }
                geometryNumber++;

                if (allTablesQueriedSuccessfully)
                {
                    geometriesQueriedSuccessfully.Add(geometry);
                }
            }

            overallStopwatch.Stop();

            var queryMessage = new StringBuilder(
                $"QUERY COMPLETE - Total Time: {overallStopwatch.Elapsed}, Query Time: {queryTime}, Get Count Time: {countTime}, Query Count: {queryCount}");
            queryMessage.AppendLine("----------------------------");

            foreach (var detail in queryDetails)
            {
                queryMessage.AppendLine($"Retrieved {detail.Value} record(s) from {detail.Key}");
            }

            await Dispatcher.InvokeAsync(() =>
            {
                Log.Text = queryMessage.ToString();
                _geometryShowingLayer.Graphics.Clear();
                foreach (var geo in geometriesQueriedSuccessfully)
                {
                    _geometryShowingLayer.Graphics.Add(new Graphic(geo, new SimpleFillSymbol
                    {
                        Color = Color.DimGray,
                        Style = SimpleFillSymbolStyle.Solid,
                        Outline = new SimpleLineSymbol
                        {
                            Color = Color.Black,
                            Style = SimpleLineSymbolStyle.Dash,
                            Width = 1
                        }
                    }));
                }
            });
        }

        private async Task TellUser(string message)
        {
            await Dispatcher.InvokeAsync(() =>
                Log.Text = message);
        }

        private void ReloadMap(object sender, RoutedEventArgs e)
        {
            MapView.Map = new Map(Basemap.CreateImagery());
        }

        private void AddLayer(object sender, RoutedEventArgs e)
        {
            AddOnlineLayer();
        }

        private async Task AddOnlineLayer()
        {
            var message = new StringBuilder();
            message.AppendLine("ONLINE DATA SOURCES LOADED:");
            message.AppendLine("----------------------------");

            try
            {
                var onlineLayer = new ArcGISMapImageLayer(new Uri(OnlineLayerTextBox.Text));
                await OnlineSources(onlineLayer, message);

                MapView.Map.OperationalLayers.Add(onlineLayer);
            }
            catch (Exception someError)
            {
                message.AppendLine($"Error occurred: {someError}");
            }

            await TellUser(message.ToString());
        }

        private async Task<IReadOnlyCollection<ServiceFeatureTable>> OnlineSources(ArcGISMapImageLayer onlineLayer, StringBuilder message)
        {
            await onlineLayer.LoadTablesAndLayersAsync();
            return onlineLayer.Tables.Concat(SourcesFromSubLayers(onlineLayer.Sublayers, message)).ToArray();
        }

        private static IEnumerable<ServiceFeatureTable> SourcesFromSubLayers(IEnumerable<ArcGISSublayer> subLayers, StringBuilder message)
        {
            foreach (var subLayer in subLayers.OfType<ArcGISMapImageSublayer>())
            {
                if (subLayer.Table != null)
                {
                    message.AppendLine(subLayer.Table.TableName);
                    yield return subLayer.Table;
                }

                if (subLayer.Sublayers.Any())
                {
                    foreach (var deeperSource in SourcesFromSubLayers(subLayer.Sublayers, message))
                    {
                        message.AppendLine(deeperSource.TableName);
                        yield return deeperSource;
                    }
                }
            }
        }

    }
}
